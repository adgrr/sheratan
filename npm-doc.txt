:::COURS NPM:::

npm -h									Aide
npm install | npm i						Installer projet depuis le net
npm install -h | npm i -h				Afficher les infos
npm -v 									Afficher la version de npm

----- Start d'un projet ------

npm init 								Commence un projet, -y pour éviter le déroulement suivant

	name:
	version:
	description:
	main:
	script:
	author:
	licence:


npm install jquery					Installer un package dans le projet -> il crée le dossier node-modules
									En ajoutant " -D" à la fin, le package sera déplcaer dans devDependencies
									Seul les développeurs auront accès
									
npm update							Mise à jour des packages (npm install met à jour aussi)


::GULP::
/**
* gulp.task - Créer tâches
* gulp.src - Pointer dossier
* gulp.dest - Destination pour déplacement
* gulp.watch - Vérfifier les modifs afin de compiler
**/

Exemple:
gulp.task(nom [dépendances], fonction);

npm install gulp -g					Installer gulp au global sur la machine -g 

Créer un fichier gulpfile.js		Dans ce fichier se trouve toute la config

npm install node-sass gulp-sass --save-dev

npm sass							Lancer la compile sass


Projet>
	build>
		css>
			styles.css
	node_modules
	src>
		sass>
			styles.scss
	index.html
	gulpfile.js
	package-lock.json
	package.json




