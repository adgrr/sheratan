'use strict';

// On déclare nos dépendances gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

// Sass utilise le compiler de nodejs
sass.compiler = require('node-sass');

// Création de ma tâche pour compiler mes fichiers sass en css
gulp.task('sass', function () {
    return gulp.src('./src/sass/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());
});

// Déplace tous les fichier html dans src
gulp.task('html', function () {
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'))
});

//Permet l'autovisualisation en cas de changement
gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    });

    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('./src/*.html', gulp.series('html')).on('change', browserSync.reload);
});

// One task to rule them all
gulp.task('default', gulp.parallel('sass', 'html', 'watch'));